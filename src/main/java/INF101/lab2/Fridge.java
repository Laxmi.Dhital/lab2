package INF101.lab2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    List<FridgeItem> items;
    private final int max_size = 20;

    public Fridge(){
        items = new ArrayList<>();
    }

    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (max_size - nItemsInFridge() > 0) {
            items.add(item);
            return true;
        }else{
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (items.contains(item)){
            items.remove(item);
        }
        else {
            throw new NoSuchElementException("No such in the Fridge");
        }
    }

    @Override
    public void emptyFridge() {
        items.removeAll(items);
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = getItems();
        items.removeAll(expiredItems);
        return expiredItems;
    }
    private List<FridgeItem> getItems(){
        List<FridgeItem> expiredItems = new ArrayList<FridgeItem>();
        for (FridgeItem item : items){
            if(item.hasExpired()) {
                expiredItems.add(item);
            }
        }
        return expiredItems;
    }
}